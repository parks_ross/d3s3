# README #

This is a sample web site showing how to create a d3 visualization that references a csv file on AWS s3, using the Aerobatic s3-proxy.

The web app can be viewed at [https://d3s3.aerobatic.io/](https://d3s3.aerobatic.io/)